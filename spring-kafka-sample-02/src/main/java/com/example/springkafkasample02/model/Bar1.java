package com.example.springkafkasample02.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author KhoiDV
 * @date 2/11/2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bar1 {
    private String bar;
}
