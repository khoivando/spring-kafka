package com.example.springkafkasample02.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author KhoiDV
 * @date 2/11/2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Foo1 {
    private String foo;
}
